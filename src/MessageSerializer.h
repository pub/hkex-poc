#pragma once

#include <chrono>
#include <string.h>
#include "MessageHeader.h"
#include <ctime>

namespace Nxtg
{
namespace HKEX
{

    template<class T>
    struct Serializer
    {
        /// @brief write object to buffer. Memory copy by default.
        /// @return number of bytes written to buffer; negative number if error accurred.
        template<class Err>
        int write( const T &obj, char *buf, unsigned buflen, Err &err )
        {
            if ( buflen < sizeof( T ) )
            {
                err.appendf( "Small buffer." );
                return -1;
            }
            memcpy( buf, &obj, sizeof( T ) );
            return sizeof( T );
        }

        /// @brief read object from buffer. Memory copy by default.
        /// @return number of bytes read; negative number if error accurred.
        template<class Err>
        int read( T &obj, const char *buf, unsigned buflen, Err &err )
        {
            if ( buflen < sizeof( T ) )
            {
                err.appendf( "Small buffer." );
                return -1;
            }
            memcpy( &obj, buf, sizeof( T ) );
            return sizeof( T );
        }
    };

    /// @brief Implement Serializer for time_point UTC.
    template<>
    struct Serializer<std::chrono::system_clock::time_point>
    {
        using ObjectType = std::chrono::system_clock::time_point;
        using EpochTimeSec = std::chrono::time_point<std::chrono::system_clock, std::chrono::seconds>;
        enum
        {
            TIMESTAMP_BUFFER_LEN = 25
        };
        static constexpr size_t Nano2SecondMultiple = 1000000000;

        /// @brief write object to buffer. Memory copy by default.
        /// @return number of bytes written to buffer; negative number if error accurred.
        template<class Err>
        int write( const ObjectType &obj, char *buf, unsigned buflen, Err &err )
        {
            if ( buflen < TIMESTAMP_BUFFER_LEN )
            {
                err.appendf( "Too short buffer to write time_point." );
                return -1;
            }
            size_t timeNanoSeconds = obj.time_since_epoch().count();
            size_t secondsPart = timeNanoSeconds / Nano2SecondMultiple;
            size_t nanoSecondsPart = timeNanoSeconds % Nano2SecondMultiple;
            EpochTimeSec timeInSec{std::chrono::seconds( secondsPart )};
            auto timet = std::chrono::system_clock::to_time_t( timeInSec );
            tm atime;
            gmtime_r( &timet, &atime );
            auto ret = strftime( buf, buflen, "%Y%m%d-%T", &atime ); // YYYYMMDD-HH:MM:SS
            if ( ret != 17 )
            {
                err.appendf( "ERROR. Failed formating tm." );
                return -1;
            }
            sprintf( buf + 17, ".%03d", int( nanoSecondsPart / 1000000 ) ); // print milli seconds
            buf[21] = '\0';

            return TIMESTAMP_BUFFER_LEN;
        }

        /// @brief read object from buffer. Memory copy by default.
        /// @return number of bytes read; negative number if error accurred.
        template<class Err>
        int read( ObjectType &obj, const char *buf, unsigned buflen, Err &err )
        {
            if ( buflen < TIMESTAMP_BUFFER_LEN )
            {
                err.appendf( "Too short buffer to write time_point." );
                return -1;
            }
            tm atime;
            memset( &atime, 0, sizeof( atime ) );
            buf = strptime( buf, "%Y%m%d-%T", &atime );
            if ( !buf )
            {
                err.appendf( "Failed parsing datetime %s", buf );
                return -1;
            }
            buf += 1; // skip "."
            size_t milliSecs = atoi( buf );

            time_t timet = timegm( &atime );
            auto timepoint = std::chrono::system_clock::from_time_t( timet );
            timepoint += std::chrono::milliseconds( milliSecs );
            obj = timepoint;

            return TIMESTAMP_BUFFER_LEN;
        }
    };

#if __cplusplus < 201703
#pragma message( "remove this block when c++17 is supported" )
    template<class T, class Err>
    struct ReadMessageBody
    {
        T &msgBody;
        const char *buf;
        unsigned buflen;
        Err &err;
        int &readPos;

        template<class FieldType>
        void operator()( typename T::FieldID fid, FieldType &field )
        {
            if ( !msgBody.is_field_set( fid ) || !err.empty() )
                return;
            Serializer<FieldType> serializer;
            auto bytesRead = serializer.read( field, buf + readPos, buflen - readPos, err );
            if ( bytesRead < 0 )
            {
                err << "Error when reading field " << T::to_cstr( fid );
                return;
            }
            readPos += bytesRead;
        }
    };
    template<class T, class Err>
    struct WriteMessageBody
    {
        const T &msgBody;
        char *buf;
        unsigned buflen;
        Err &err;
        int &writePos;

        template<class FieldType>
        void operator()( typename T::FieldID fid, FieldType &field )
        {
            if ( !msgBody.is_field_set( fid ) )
                return;
            Serializer<FieldType> serializer;
            auto bytesWrite = serializer.write( field, buf + writePos, buflen - writePos, err );
            if ( bytesWrite < 0 )
            {
                err << "Error when writing field " << T::to_cstr( fid );
                return;
            }
            writePos += bytesWrite;
        }
    };
#endif
    template<class T>
    struct MessageSerializer
    {
        template<size_t N>
        int read( T &msgBody, const char *buf, unsigned buflen, FixedStr<N> &err )
        {
            int readPos = read_header( static_cast<MessageHeader &>( msgBody ), buf, buflen, err );
            if ( readPos <= 0 )
                return readPos;

                //- deserialize message body
#if __cplusplus < 201703
#pragma message( "remove this block when c++17 is supported" )
            ReadMessageBody<T, FixedStr<N>> readMsgBody{msgBody, buf, buflen, err, readPos};
            msgBody.foreach_field( readMsgBody );
#else
            msgBody.foreach_field( [&]( typename T::FieldID fid, auto &field ) {
                using FieldType = typename std::remove_reference<decltype( field )>::type;
                if ( !msgBody.is_field_set( fid ) || !err.empty() )
                    return;
                Serializer<FieldType> serializer;
                auto bytesRead = serializer.read( field, buf + readPos, buflen - readPos, err );
                if ( bytesRead < 0 )
                {
                    err << "Error when reading field " << T::to_cstr( fid );
                    return;
                }
                readPos += bytesRead;
            } );
#endif
            if ( !err.empty() )
                return -1;

            Serializer<Checksum> serializer;
            auto bytesRead = serializer.read( msgBody.trailer, buf + readPos, buflen - readPos, err );
            if ( bytesRead <= 0 )
            {
                err << "Error when reading trailer.";
                return -1;
            }
            readPos += bytesRead;
            assert( readPos == msgBody.length );

            return readPos;
        }

        // @brief write Object into buffer. MessageHeader::length is auto populated.
        template<size_t N>
        int write( const T &msgBody, char *buf, unsigned buflen, FixedStr<N> &err )
        {
            if ( buflen < sizeof( MessageHeader ) )
            {
                err << "Unable to write to small buffer " << buflen << " < headersize:" << sizeof( MessageHeader );
                return -1;
            }
            memcpy( buf, &msgBody, sizeof( MessageHeader ) );
            int writePos = sizeof( MessageHeader );

            //- serialize message body
#if __cplusplus < 201703
#pragma message( "remove this block when c++17 is supported" )
            WriteMessageBody<T, FixedStr<N>> writeMessageBody{msgBody, buf, buflen, err, writePos};
            msgBody.foreach_field( writeMessageBody );
#else
            msgBody.foreach_field( [&]( typename T::FieldID fid, auto &field ) {
                using FieldType = typename std::remove_reference<decltype( field )>::type;
                if ( !msgBody.is_field_set( fid ) )
                    return;
                Serializer<FieldType> serializer;
                auto bytesWrite = serializer.write( field, buf + writePos, buflen - writePos, err );
                if ( bytesWrite < 0 )
                {
                    err << "Error when writing field " << fid;
                    return;
                }
                writePos += bytesWrite;
            } );
#endif
            if ( !err.empty() )
                return -1;

            Serializer<Checksum> serializer;
            auto bytesWrite = serializer.write( msgBody.trailer, buf + writePos, buflen - writePos, err );
            if ( bytesWrite < 0 )
            {
                err << "Error when writing trailer.";
                return -1;
            }
            writePos += bytesWrite;
            cast_to_message_header( buf, buflen )->length = writePos;

            // msgBody.length = writePos;  // set length
            return writePos;
        }

        int read_header( MessageHeader &header, const char *buf, unsigned buflen, ErrMessage &err )
        {
            if ( buflen < sizeof( MessageHeader ) )
            {
                err << "small buffer buflen " << buflen << " < header size:" << sizeof( MessageHeader );
                return -1;
            }
            memcpy( &header, buf, sizeof( MessageHeader ) );
            if ( header.startOfMessage != MessageHeader::START_OF_MESSAGE )
            {
                err.appendf( "startOfMessage != %x.", MessageHeader::START_OF_MESSAGE );
                return -1;
            }
            if ( header.messageType >= MessageType::END_ )
            {
                err.appendf( "Unknown message type %d.", header.messageType );
                return -1;
            }
            return sizeof( MessageHeader );
        }
    };

} // namespace HKEX
} // namespace Nxtg
