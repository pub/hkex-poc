#pragma once
#include "FixedStr.h"
#include <string>

namespace Nxtg
{
namespace HKEX
{

    enum Const
    {
        COMPID_LEN = 12,
        IPADDRESS_LEN = 16,
        PASSWORD_LEN = 450
    };

    using Text = std::string;

    enum class MessageType : uint8_t
    {
        Heartbeat = 0,
        TestRequest,
        ResendRequest,
        Reject,
        SequenceReset,
        Logon,
        Logout,
        LookupRequest,
        LookupResponse,
        BusinessMessageReject,
        ExecutionReport,
        NewOrder,
        AmendRequest,
        CancelRequest,
        MassCancelRequest,
        OrderMassCancelReport,
        Quote,
        QuoteCancel,
        QuoteStatusReport,
        TradeCaptureReport,
        TradeCaptureReportAck,
        OBOCancelRequest,
        OBOMassCancelRequest,
        ThrottleEntitlementRequest,
        ThrottleEntitlementResponse,
        PartyEntitlementsRequest,
        PartyEntitlementsReport,
        END_
    };

    struct FieldsPresenceMap
    {
        uint8_t bits;
    };

    struct MessageHeader
    {
        uint8_t startOfMessage; //!< Indicates the starting point of a message. Always set to the ASCII STX character (0x02).
        uint16_t length; //!< length of entire message .
        MessageType messageType; //!< 0 – No (original transmission), 1 – Yes (possible duplicate.
        uint32_t sequenceNumber;
        uint8_t possDup; //!< Indicates possible retransmission of message with this sequence number.
        uint8_t possResend; //!< Indicates that message may contain information that has been sent under another sequence number:  0 – No (original
                            //!< transmission) ,  1 – Yes (possible resend)	FieldsPresenceMap presenceMap;
        FixedStr<COMPID_LEN> compID;
        //  FieldsPresenceMap fieldsMap;
    };

    struct MessageTrailer
    {
        uint32_t checksum; //!< CRC32C based checksum. Polynomial used - 0x1EDC6F41.
    };

    /// @name LookupService Lookup Service Message Bodies
    ///@{
    struct LookupRequest
    {
        MessageHeader header;
        uint8_t typeOfService; //!< The type of service required by the client: 1 = Order Input.
        uint8_t protocolType; //!< The type of protocol required by the client in order to connect to the specified service: 1 = Binary
    };

    struct LookupResponse
    {
        uint8_t status; //!< Indicates whether the Lookup Request was accepted or rejected by the OCG: 0 = Accepted, 1 = Rejected
        uint8_t lookupRejectCode; //!< If request is rejected then a code to identify the rejection: 0 = Invalid Client, 1 = Invalid service type, 2
                                  //!< = Invalid Protocol, 3 = Client is blocked, 4 = Other.
        Text reason; //!< Textual reason for the Lookup Request rejection.
        FixedStr<IPADDRESS_LEN> primaryIP; //!< IP Address of the primary service in case of a successful lookup.
        uint16_t primaryPort; //!< Port number of the primary service in case of a successful lookup.
        FixedStr<IPADDRESS_LEN> secondaryIP;
        uint16_t secondaryPort;
    };
    ///@}

    /// @name AdministrativeMessages Administrative Message Bodies
    ///@{
    struct Logon
    {
        FixedStr<PASSWORD_LEN> password; //!< Encrypted Password assigned to the Comp ID.
        FixedStr<PASSWORD_LEN> newPassword; //!< The type of service required by the client: 1 = Order Input.
        uint8_t protocolType; //!< The type of protocol required by the client in order to connect to the specified service: 1 = Binary
    };

    struct LogonResponse
    {
    };
    ///@}

} // namespace HKEX
} // namespace Nxtg
