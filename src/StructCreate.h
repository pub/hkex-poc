#pragma once
#include "BitMap.h"
#include <type_traits>
#include <unordered_map>


#define S_SELECT_SECOND( first, second, ... ) second
#define S_SELECT_FIRST( first, ... ) first

#define S_COMMA ,
#define S_NOTHING



////======== DEFINE_ENUM_CLASS  ======================
/**
#define Color_(T, D) T(White, 1) D T(Red, 2)
DEFINE_ENUM_CLASS( Color, int )

  will be parsed to:
enum class Color : int{ White = 1, Red = 2, END_ };

DEFINE_AUTOENUM( Color )
  enum Color { White, Red, END_ };


DEFINE_ENUM_STR( Color )
  will be parsed to:

const char* to_cstr(Color value){
    constexpr SIZE = static_cast<size_t>(static_cast<size_t>(Color::END_)) ;
    struct my {
        const char * m_values[SIZE];
        my (){
           m_values[static_cast<size_t>(White)] = "White";
           m_values[static_cast<size_t>(Red)] = "Red";
        }
    };
    static my s_my{};
    auto i = static_cast<size_t>(value) ;
    return i < SIZE ? my.m_values[i]: nullptr;
}
bool from_cstr( Color &value, const char *name ){
    constexpr SIZE = static_cast<size_t>(static_cast<size_t>(Color::END_)) ;
    struct my {
        static std::unordred_map<const char *, Color> m_values;
        my (){
           m_values["White"] = static_cast<size_t>(White);
           m_values["Red"] = static_cast<size_t>(Red);
        }
    };
    static my s_my{};
    auto it = my.m_values.find(name);
    if( it == my.m_values.end() ) return false;
    value = it->second;
    return true;
}
 * ****************/


#define E_MAKE_TWO( name, value ) name = value

#define E_MAP_ENUM2TR( name, ... ) m_values[static_cast<size_t>( EnumType::name )] = #name;
#define E_MAP_STR2ENUM( name, ... ) m_values[#name] = EnumType::name;


#define DEFINE_ENUM_VALUES( T ) T##_( E_MAKE_TWO, S_COMMA )
#define DEFINE_AUTOENUM_VALUES( T ) T##_( S_SELECT_FIRST, S_COMMA )

#define DEFINE_ENUM_CLASS( T, BASE )                                                                                                                \
    enum class T : BASE                                                                                                                             \
    {                                                                                                                                               \
        DEFINE_ENUM_VALUES( T ),                                                                                                                    \
        END_                                                                                                                                        \
    };


#define DEFINE_AUTOENUM( T )                                                                                                                        \
    enum T                                                                                                                                          \
    {                                                                                                                                               \
        DEFINE_AUTOENUM_VALUES( T ),                                                                                                                \
        END_                                                                                                                                        \
    };

#define DEFINE_ENUM_STR( T ) DEFINE_ENUM_TO_CSTR( T, T ) DEFINE_ENUM_FROM_CSTR( T, T )

#define DEFINE_ENUM_TO_CSTR( T, E, ... )                                                                                                            \
    template<class U>                                                                                                                               \
    __VA_ARGS__ typename std::enable_if<std::is_same<U, E>::value, const char *>::type to_cstr( const U &value )                                    \
    {                                                                                                                                               \
        constexpr size_t SIZE = static_cast<size_t>( static_cast<size_t>( E::END_ ) );                                                              \
        struct my                                                                                                                                   \
        {                                                                                                                                           \
            using EnumType = E;                                                                                                                     \
            const char *m_values[SIZE];                                                                                                             \
            my()                                                                                                                                    \
            {                                                                                                                                       \
                T##_( E_MAP_ENUM2TR, S_NOTHING )                                                                                                    \
            }                                                                                                                                       \
        };                                                                                                                                          \
        static my s_my{};                                                                                                                           \
        auto i = static_cast<size_t>( value );                                                                                                      \
        return i < SIZE ? s_my.m_values[i] : nullptr;                                                                                               \
    }


#define DEFINE_ENUM_FROM_CSTR( T, E, ... )                                                                                                          \
    template<class U>                                                                                                                               \
    __VA_ARGS__ typename std::enable_if<std::is_same<U, E>::value, bool>::type from_cstr( U &value, const char *name )                              \
    {                                                                                                                                               \
        struct my                                                                                                                                   \
        {                                                                                                                                           \
            using EnumType = E;                                                                                                                     \
            std::unordered_map<const char *, E> m_values;                                                                                           \
            my()                                                                                                                                    \
            {                                                                                                                                       \
                T##_( E_MAP_STR2ENUM, S_NOTHING )                                                                                                   \
            }                                                                                                                                       \
        };                                                                                                                                          \
        static my s_my{};                                                                                                                           \
        auto it = s_my.m_values.find( name );                                                                                                       \
        if ( it == s_my.m_values.end() )                                                                                                            \
            return false;                                                                                                                           \
        value = it->second;                                                                                                                         \
        return true;                                                                                                                                \
    }


///================= DEFINE_CLASS ====================================
/******************************************************
 *
Usage:

#define Request_(T, D) T(id, int) D T(length, int)
STRUCT_CREATE( Request )

will be parsed to:

struct Request
{
    enum FieldID{id, length, END_, MaxFieldID_ = END_};

    const int& get_id() const {
        return m_id;
    }

    template<class T_t>
    Request& set_id(T_t&& v){
        m_id = std::forward<T_t>(v);
        m_fieldPresenceMap.set(id);
        return this;
    }
    const int &get_length() const {
        return m_legnth;
    }

    template<class T_t>
    Request& set_length(T_t&& v){
        m_length = std::forward<T_t>(v);
        m_fieldPresenceMap.set(length);
        retrun *this;
    }

    bool is_field_set(FieldID id) {
        return m_fieldPresenceMap.test(id);
    }

    Request& reset_field(FieldID id) {
        m_fieldPresenceMap.reset(id);
        return *this;
    }
    template<class Func>
    void foreach_field(Func&& func) {
        func( id, m_id );
        func( length, m_length );
    }
    template<class U>
    const U *get_field_by_id(FieldID fid) const;

    void *get_field_address(FieldID ifd) const;

    std::bitset<END_> m_fieldPresenceMap;
    int m_id;
    int m_length;
};
 *
 * *********************************************/

#define S_DEF_DECLARE_FIELD( name, type ) type m_##name;

#define S_DEF_FUNC_SET( name, type )                                                                                                                \
    template<class T_t>                                                                                                                             \
    void set_##name( T_t &&v )                                                                                                                      \
    {                                                                                                                                               \
        m_##name = std::forward<T_t>( v );                                                                                                          \
        m_fieldPresenceMap.set( static_cast<size_t>( FieldID::name ) );                                                                             \
    }

#define S_DEF_FUNC_GET( name, type )                                                                                                                \
    const type &get_##name() const                                                                                                                  \
    {                                                                                                                                               \
        return m_##name;                                                                                                                            \
    }

#define S_DEF_FUNC_IS_FIELD_SET( name, type )                                                                                                       \
    bool is_##name##_set() const                                                                                                                    \
    {                                                                                                                                               \
        return m_fieldPresenceMap.test( static_cast<size_t>( FieldID::name ) );                                                                     \
    }

#define S_DEF_FUNC_RESET_FIELD( name, typee )                                                                                                       \
    void reset_##name()                                                                                                                             \
    {                                                                                                                                               \
        m_fieldPresenceMap.reset( static_cast<size_t>( FieldID::name ) );                                                                           \
    }

#define S_DEF_FUNC_CALL_FIELD( name, type ) func( FieldID::name, m_##name );


#define S_DEF_GET_FIELD_BY_ID( name, type )                                                                                                         \
    if ( fid == FieldID::name )                                                                                                                     \
        return std::is_same<U, type>::value ? &m_##name : nullptr;

#define S_FIELD_ADDRESS( name, type ) const_cast<void *>( reinterpret_cast<const void *>( &m_##name ) )


#define STRUCT_FIELDS( T )                                                                                                                          \
    enum class FieldID                                                                                                                              \
    {                                                                                                                                               \
        T##_( S_SELECT_FIRST, S_COMMA ),                                                                                                            \
        END_,                                                                                                                                       \
        MaxFieldID_ = END_                                                                                                                          \
    };                                                                                                                                              \
    DEFINE_ENUM_TO_CSTR( T, FieldID, static )                                                                                                       \
    DEFINE_ENUM_FROM_CSTR( T, FieldID, static )                                                                                                     \
    T##_( S_DEF_DECLARE_FIELD, S_NOTHING ) T##_( S_DEF_FUNC_GET, S_NOTHING ) T##_( S_DEF_FUNC_SET, S_NOTHING )                                      \
            T##_( S_DEF_FUNC_IS_FIELD_SET, S_NOTHING ) T##_( S_DEF_FUNC_RESET_FIELD, S_NOTHING ) bool                                               \
            is_field_set( FieldID fid ) const                                                                                                       \
    {                                                                                                                                               \
        return m_fieldPresenceMap.test( static_cast<size_t>( fid ) );                                                                               \
    }                                                                                                                                               \
    void reset_field( FieldID fid )                                                                                                                 \
    {                                                                                                                                               \
        m_fieldPresenceMap.reset( static_cast<size_t>( fid ) );                                                                                     \
    }                                                                                                                                               \
    template<class Func>                                                                                                                            \
    void foreach_field( Func &&func )                                                                                                               \
    {                                                                                                                                               \
        T##_( S_DEF_FUNC_CALL_FIELD, S_NOTHING )                                                                                                    \
    }                                                                                                                                               \
    template<class Func>                                                                                                                            \
    void foreach_field( Func &&func ) const                                                                                                         \
    {                                                                                                                                               \
        T##_( S_DEF_FUNC_CALL_FIELD, S_NOTHING )                                                                                                    \
    }                                                                                                                                               \
    template<class U>                                                                                                                               \
    const U *get_field_by_id( FieldID fid ) const                                                                                                   \
    {                                                                                                                                               \
        T##_( S_DEF_GET_FIELD_BY_ID, S_NOTHING );                                                                                                   \
        return nullptr;                                                                                                                             \
    }                                                                                                                                               \
    void *get_field_address( FieldID fid ) const                                                                                                    \
    {                                                                                                                                               \
        static void *s_fields[] = {T##_( S_FIELD_ADDRESS, S_COMMA )};                                                                               \
        return fid >= FieldID::MaxFieldID_ ? nullptr : s_fields[static_cast<size_t>( fid )];                                                        \
    }


#define DEINE_STRUCT( T )                                                                                                                           \
    struct T                                                                                                                                        \
    {                                                                                                                                               \
        BitMap<64> m_fieldPresenceMap;                                                                                                              \
        STRUCT_FIELDS( T )                                                                                                                          \
    };
