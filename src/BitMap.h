#pragma once
#include <stdint.h>
#include <assert.h>
#include <type_traits>

template<size_t NBYTES>
struct IntegerType;

template<>
struct IntegerType<1>
{
    using type = int8_t;
};
template<>
struct IntegerType<2>
{
    using type = int16_t;
};
template<>
struct IntegerType<4>
{
    using type = int32_t;
};
template<>
struct IntegerType<8>
{
    using type = int64_t;
};

template<size_t NBITS>
class BitMap
{
public:
    static_assert( NBITS <= 64, "NBITS <=64 " );
    using size_type = unsigned;
    static constexpr size_type NBits = NBITS;
    static constexpr size_type NBytes = NBits / 8 + ( ( NBits % 8 == 0 ) ? 0 : 1 );
    static constexpr size_type NAllBits = NBytes * 8;

    using underlying_type = typename std::make_unsigned<typename IntegerType<NBytes>::type>::type;

public:
    bool test( size_type n ) const
    {
        return operator[]( n );
    }

    bool operator[]( size_type n ) const
    {
        assert( n < NBITS && "n out of range" );
        return get_bit( n );
    }

    void reset( size_type n )
    {
        assert( n < NBITS && "n out of range" );
        m_data &= ~( underlying_type( 1 ) << ( NAllBits - n - 1 ) );
    }

    void reset()
    {
        m_data = underlying_type( 0 );
    }

    void set( size_type n, bool value = true )
    {
        assert( n < NBITS && "n out of range" );
        if ( value )
            set_bit( n );
        else
            reset( n );
    }
    void set()
    {
        m_data = ~underlying_type( 0 );
    }
    void flip( size_type n )
    {
        set( n, !test( n ) );
    }
    void flip()
    {
        m_data = ~m_data;
    }
    underlying_type get_underlying() const
    {
        return m_data;
    }
    void set_bit( size_type n )
    {
        m_data |= ( underlying_type( 1 ) << ( NAllBits - n - 1 ) );
    }
    underlying_type get_bit( size_t n ) const
    {
        return m_data & ( underlying_type( 1 ) << ( NAllBits - n - 1 ) );
    }

    template<size_t N>
    bool operator==( const BitMap<N> &amap ) const
    {
        return NBits == BitMap<N>::NBits && m_data == amap.get_underlying();
    }

private:
    underlying_type m_data;
} __attribute__( ( packed ) );
