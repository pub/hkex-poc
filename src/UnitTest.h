#include <iostream>
#include <chrono>
#include <sstream>

#define TEST_FUNC( funcName, ... )                                                                                                                  \
    static void funcName();                                                                                                                         \
    static struct funcName##_runner                                                                                                                 \
    {                                                                                                                                               \
        funcName##_runner()                                                                                                                         \
        {                                                                                                                                           \
            std::cout << "++ Starting test " << #funcName << std::endl;                                                                             \
            auto timeStart = std::chrono::steady_clock::now();                                                                                      \
            funcName();                                                                                                                             \
            auto timeDiff = std::chrono::steady_clock::now() - timeStart;                                                                           \
            std::cout << "-- Test ended " << #funcName << ", time elapsed (ns): " << timeDiff.count() << std::endl;                                 \
        }                                                                                                                                           \
    } s_##funcName##_runner__;                                                                                                                      \
    static void funcName()

#define T_STRINGIFY( x ) #x
#define T_TOSTRING( x ) T_STRINGIFY( x )

struct UnitTestException : public std::runtime_error
{
    UnitTestException( const char *err ) : std::runtime_error( err )
    {
    }
};

//#define REQUIRE( expr, ... ) assert(expr)

#define REQUIRE( expr, ... )                                                                                                                        \
    do                                                                                                                                              \
    {                                                                                                                                               \
        if ( !( expr ) )                                                                                                                            \
        {                                                                                                                                           \
            std::stringstream ss;                                                                                                                   \
            ss << "EvaluationError in " __FILE__ ":" T_TOSTRING( __LINE__ ) ". Expr:\"" #expr << "\", Desc:\"" __VA_ARGS__ << "\"";                 \
            throw UnitTestException( ss.str().c_str() );                                                                                            \
        }                                                                                                                                           \
    } while ( false )
