#pragma once
#include "MessageHeader.h"
#include <chrono>
#include "Decimal.h"
#include "MessageSerializer.h"

namespace Nxtg
{
namespace HKEX
{

    using IPAddress = FixedStr<16>;
    using Password = FixedStr<450>;
    using Text = FixedStr<50>;
    using ClientOrderID = FixedStr<21>;
    using OrderID = FixedStr<21>;
    using SecurityID = FixedStr<21>;
    using BrokerID = FixedStr<12>;
    using SecurityExchange = FixedStr<5>;
    using BrokerLocationID = FixedStr<11>;
    using TransactionTime = FixedStr<25>;
    using ExecutionID = FixedStr<21>;
    using Reason = FixedStr<75>;

    using SteadyClock = std::chrono::steady_clock;
    using SystemClock = std::chrono::system_clock;
    using TimeStamp = std::chrono::time_point<SteadyClock>;
    using EpochTime = SystemClock::time_point;
    using EpochTimeSec = std::chrono::time_point<SystemClock, std::chrono::seconds>;

    using Price = Decimal<int64_t, 8>;
    using OrderQuantity = Decimal<int64_t, 8>;

/// @name LookupService Lookup Service Message Bodies
///@{
#define LookupRequest_( T, D ) T( typeOfService, uint8_t ) D T( protocolType, uint8_t )
    DEINE_STRUCT_WITH_BASE( LookupRequest )

#define LookupResponse_( T, D )                                                                                                                     \
    T( status, uint8_t )                                                                                                                            \
    D T( lookupRejectCode, uint8_t ) D T( reason, Text ) D T( primaryIP, IPAddress ) D T( primaryPort, uint16_t ) D T( secondaryIP, IPAddress )     \
            D T( secondaryPort, uint16_t )
    DEINE_STRUCT_WITH_BASE( LookupResponse )
    ///@}

    /// @name AdministrativeMessages Administrative Message Bodies
    ///@{
    enum class SessionStatus : uint8_t
    {
        SessionActive = 0,
        SessionPasswordChange = 1,
        SessionPasswordChangeDueToExpire = 2,
        NewSessionPasswordDoesNotComplyWithThePolicy = 3,
        SessionLogoutComplete = 4,
        InvalidUserNameOrPassword = 5,
        AccountLocked = 6,
        LogonsAreNotAllowedAtThisTime = 7,
        PasswordExpired = 8,
        PasswordChangeIsRequired = 100,
        Other = 101
    };
    enum class TestMessageIndicator : uint8_t
    {
        ProductionMode = 0,
        TestMode = 1
    };
#define Logon_( T, D )                                                                                                                              \
    T( password, Password )                                                                                                                         \
    D T( newPassword, Password ) D T( nextExceptedMessageSequence, uint32_t ) D T( sessionStatus, SessionStatus ) D T( text, Text )                 \
            D T( testMessageIndicator, TestMessageIndicator )
    DEINE_STRUCT_WITH_BASE( Logon )

#define Logout_( T, D ) T( logoutText, FixedStr<75> ) D T( sessionStatus, SessionStatus )
    DEINE_STRUCT_WITH_BASE( Logout )

#define Heartbeat_( T, D ) T( referenceTestRequestID, uint16_t )
    DEINE_STRUCT_WITH_BASE( Heartbeat )

#define TestRequest_( T, D ) T( testRequestID, uint16_t )
    DEINE_STRUCT_WITH_BASE( TestRequest )

#define ResendRequest_( T, D ) T( startSequence, uint32_t ) D T( endSequence, uint32_t )
    DEINE_STRUCT_WITH_BASE( ResendRequest )

#define Reject_( T, D )                                                                                                                             \
    T( messageRejectCode, uint16_t )                                                                                                                \
    D T( reason, Reason ) D T( referenceMessageType, MessageType ) D T( referenceFieldName, FixedStr<50> ) D T( referenceSequenceNumber, uint32_t ) \
            D T( clientOrderID, ClientOrderID )
    DEINE_STRUCT_WITH_BASE( Reject )

    enum class GapFill : char
    {
        Reset = 'N',
        GapFill = 'Y'
    };

#define SequenceReset_( T, D ) T( gapFill, GapFill ) D T( newSequenceNumber, uint32_t )
    DEINE_STRUCT_WITH_BASE( SequenceReset )

    ///@}

    /// @name BusinessMessages Business Message Bodies
    ///@{

    enum class SecurityIDSource : uint8_t
    {
        ExchangeSymbol = 8
    };

    static const char *const securityExchangeHK = "XHKG";

    enum class Side : uint8_t
    {
        Buy = 1,
        Sell = 2,
        SellShort = 3
    };
    enum class OrderType : uint8_t
    {
        Market = 1,
        Limit = 2
    };
    enum class TimeInForce : uint8_t
    {
        Day = 0,
        IOC = 3,
        FOK = 4,
        AtCrossing = 9
    };
    enum class PositionEffect : uint8_t
    {
        Close = 1 // Applicable only if Side = Buy
    };
    using OrderRestrictions = FixedStr<21>;

    enum class OrderCapacity : uint8_t
    {
        Agency = 1,
        Principal = 3
    };
    using ExecutionInstructions = FixedStr<21>;

    enum class LotType : uint8_t
    {
        OddLot = 1,
        RoundLot = 2
    };

#define NewOrder_( T, D )                                                                                                                           \
    T( clientOrderID, ClientOrderID )                                                                                                               \
    D T( submittingBrokerID, BrokerID ) D T( securityID, SecurityID ) D T( securityIDSource, SecurityIDSource )                                     \
            D T( securityExchange, SecurityExchange ) D T( brokerLocationID, BrokerLocationID ) D T( transactionTime, TransactionTime )             \
                    D T( side, Side ) D T( orderType, OrderType ) D T( price, Price ) D T( orderQuantity, OrderQuantity ) D T( tif, TimeInForce )   \
                            D T( positionEffect, PositionEffect ) D T( orderRestrictions, OrderRestrictions ) D T( maxPriceLevels, uint8_t )        \
                                    D T( orderCapacity, OrderCapacity ) D T( text, Text ) D T( executionInstructions, ExecutionInstructions )       \
                                            D T( disclosureInstructions, uint16_t ) D T( lotType, LotType )
    DEINE_STRUCT_WITH_BASE( NewOrder )

#define AmendOrder_( T, D )                                                                                                                         \
    T( clientOrderID, ClientOrderID )                                                                                                               \
    D T( submittingBrokerID, BrokerID ) D T( securityID, SecurityID ) D T( securityIDSource, SecurityIDSource )                                     \
            D T( securityExchange, SecurityExchange ) D T( brokerLocationID, BrokerLocationID ) D T( transactionTime, TransactionTime )             \
                    D T( side, Side ) D T( originalClientOrderI, ClientOrderID ) D T( orderID, OrderID ) D T( orderType, OrderType )                \
                            D T( price, Price ) D T( orderQuantity, OrderQuantity ) D T( tif, TimeInForce ) D T( positionEffect, PositionEffect )   \
                                    D T( orderRestrictions, OrderRestrictions ) D T( maxPriceLevels, uint8_t ) D T( orderCapacity, OrderCapacity )  \
                                            D T( text, Text ) D T( executionInstructions, ExecutionInstructions )                                   \
                                                    D T( disclosureInstructions, uint16_t )
    DEINE_STRUCT_WITH_BASE( AmendOrder )

#define CancelOrder_( T, D )                                                                                                                        \
    T( clientOrderID, ClientOrderID )                                                                                                               \
    D T( submittingBrokerID, BrokerID ) D T( securityID, SecurityID ) D T( securityIDSource, SecurityIDSource )                                     \
            D T( securityExchange, SecurityExchange ) D T( brokerLocationID, BrokerLocationID ) D T( transactionTime, TransactionTime )             \
                    D T( side, Side ) D T( originalClientOrderID, ClientOrderID ) D T( orderID, OrderID ) D T( text, Text )
    DEINE_STRUCT_WITH_BASE( CancelOrder )

    enum class MassCancelRequestType : uint8_t
    {
        CancelOrdersForSecurity = 1,
        CancelAllOrders = 7,
        CancelOrdersForAMarketSegment = 9
    };

    ///--- On Behalf of Order

    using MarketSegmentID = FixedStr<20>;

#define MassCancel_( T, D )                                                                                                                         \
    T( clientOrderID, ClientOrderID )                                                                                                               \
    D T( submittingBrokerID, BrokerID ) D T( securityID, SecurityID ) D T( securityIDSource, SecurityIDSource )                                     \
            D T( securityExchange, SecurityExchange ) D T( brokerLocationID, BrokerLocationID ) D T( transactionTime, TransactionTime )             \
                    D T( side, Side ) D T( massCancelRequestType, MassCancelRequestType ) D T( marketSegmentID, MarketSegmentID )
    DEINE_STRUCT_WITH_BASE( MassCancel )

#define OBOCancelRequest_( T, D )                                                                                                                   \
    T( clientOrderID, ClientOrderID )                                                                                                               \
    D T( submittingBrokerID, BrokerID ) D T( securityID, SecurityID ) D T( securityIDSource, SecurityIDSource )                                     \
            D T( securityExchange, SecurityExchange ) D T( brokerLocationID, BrokerLocationID ) D T( transactionTime, TransactionTime )             \
                    D T( side, Side ) D T( originalClientOrderID, ClientOrderID ) D T( orderID, OrderID ) D T( owningBrokerID, BrokerID )           \
                            D T( text, Text )
    DEINE_STRUCT_WITH_BASE( OBOCancelRequest )

#define OBOMassCancelRequest_( T, D )                                                                                                               \
    T( clientOrderID, ClientOrderID )                                                                                                               \
    D T( submittingBrokerID, BrokerID ) D T( securityID, SecurityID ) D T( securityIDSource, SecurityIDSource )                                     \
            D T( securityExchange, SecurityExchange ) D T( brokerLocationID, BrokerLocationID ) D T( transactionTime, TransactionTime )             \
                    D T( side, Side ) D T( massCancelRequestType, MassCancelRequestType ) D T( marketSegmentID, MarketSegmentID )                   \
                            D T( owningBrokerID, BrokerID )
    DEINE_STRUCT_WITH_BASE( OBOMassCancelRequest )

    ///--- Execution Report

    enum class OrderStatus : uint8_t
    {
        New = 0,
        PartiallyFilled = 1,
        Filled = 2,
        Cancelled = 4,
        PendingCancel = 6,
        Rejected = 8,
        PendingNew = 10,
        Expired = 12,
        PendingAmend = 14
    };

    enum class ExecType : char
    {
        New = '0',
        Cancel = '4',
        Amend = '5',
        Reject = '8',
        Expire = 'C',
        Trade = 'F',
        TradeCancel = 'H',
        Triggered = 'L',
        CancelReject = 'X',
        AmendReject = 'Y'
    };

    enum class OrderRejectCode : uint16_t
    {
        OrderExeedLimit = 3,
        DuplicateOrder = 6,
        IncorretQty = 13,
        PriceExceedsCurrentPriceBand = 16
    };

    // @brief applicable when ExecType = 4(Cancel)
    enum class ExecRestatementReason : uint16_t
    {
        CancelOnTradingHalt = 6,
        MarketOperation = 8,
        UUnsolicitedCancel = 100,
        OnBehalfOfSingleCanle = 101,
        OnBehalfOfMassCancel = 102,
        MassCancelledByBloker = 103,
        CancelOnDisconnect = 104,
        CancelDueToBlokerSyspended = 105,
        CancelDueToExchangeParticipantSuspended = 106,
        SystemCancel = 107
    };

    enum class CancelRejectCode : uint16_t
    {
        TooLateToCancel = 0,
        UnknownOrder = 1,
        OrderAlreadyInPending = 3,
        DuplicateClientOrderID = 6,
        Other = 99
    };

    enum class AmendRejectCode : uint16_t
    {
        TooLateToAmend = 0,
        UnknownOrder = 1,
        OrderAlreadyInPending = 3,
        DuplicateClientOrderID = 6,
        ReferencePriceNotAvailable = 100,
        PriceExceedsCurrentPriceBandNoOverride = 101,
        PriceExceedsCurrentPriceBand = 102,
        NotionalValueExceedsThreshold = 103
    };

    enum class MatchType : uint8_t
    {
        AutoMatch = 4,
        CrossAuction = 5
    };

    enum class OrderCategory : uint8_t
    {
        InternalCrossOrder = 1
    };

    using TradeMatchID = FixedStr<25>;

    enum class ExchangeTradeType : char
    {
        ManualTrade = 'M',
        ManualNonStandardPrice = 'S',
        SpecialLotTrade = 'Q',
        OddLotTrade = 'P',
        PreviosDayTrade = 'R',
        OverseasTrade = 'V',
        SpecialLotSemiAutoMatching = 'E',
        OddLotSemiAutoMatching = 'O'
    };

#define ExecutionReport_( T, D )                                                                                                                    \
    T( clientOrderID, ClientOrderID )                                                                                                               \
    D T( submittingBrokerID, BrokerID ) D T( securityID, SecurityID ) D T( securityIDSource, SecurityIDSource ) D T(                                \
            securityExchange, SecurityExchange ) D T( brokerLocationID, BrokerLocationID ) D T( transactionTime, TransactionTime )                  \
            D T( side, Side ) D T( originalClientOrderID, ClientOrderID ) D T( orderId, OrderID ) D T( owningBrokerID, BrokerID ) /*<-10*/ D T(     \
                    orderType, OrderType ) D T( price, Price ) D T( orderQuantity, OrderQuantity ) D T( tif, TimeInForce )                          \
                    D T( positionEffect, PositionEffect ) D T( orderRestrictions, OrderRestrictions ) D T( maxPriceLevels, uint8_t )                \
                            D T( orderCapacity, OrderCapacity ) D T( text, Text ) D T( reason, Reason ) /*<-20*/ D T( executionID, ExecutionID )    \
                                    D T( orderStatus, OrderStatus ) D T( execType, ExecType ) D T( cumulativeQuantity, OrderQuantity )              \
                                            D T( leavesQuantity, OrderQuantity ) D T( orderRejectCode, OrderRejectCode ) D T( lotType, LotType )    \
                                                    D T( execRestatementReason, ExecRestatementReason ) D T( cancelRejectCode, CancelRejectCode )   \
                                                            D T( matchType, MatchType ) /*<-30*/ D T( counterpartyBrokerID, BrokerID )              \
                                                                    D T( executionQuantity, OrderQuantity ) D T( executionPrice, Price )            \
                                                                            D T( referenceExecutionID, ExecutionID )                                \
                                                                                    D T( orderCategory, OrderCategory )                             \
                                                                                            D T( amendRejectCode, AmendRejectCode ) /*<-36*/ D      \
                                                                                            T( undefined37, int ) D T( tradeMatchID, TradeMatchID ) \
                                                                                                    D T( exchangeTradeType, ExchangeTradeType )

    DEINE_STRUCT_WITH_BASE( ExecutionReport )

    ///@}

} // namespace HKEX
} // namespace Nxtg
