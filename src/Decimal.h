#pragma
#include <cmath>

/// @brief integer represented Decimal
template<class UnderlyingType, size_t NumOfDecimalsT, UnderlyingType NANValueT = 0, bool HasNANRepresentation = false>
class Decimal
{
public:
    using this_type = Decimal;
    using underlying_type = UnderlyingType;
    static constexpr size_t NumOfDecimals = NumOfDecimalsT;
    static constexpr bool HasNAN = HasNANRepresentation;
    static constexpr underlying_type NANValue = NANValueT;
    static constexpr underlying_type Multiplier = NumOfDecimalsT ? ( 10 * NumOfDecimalsT ) : 1;

    Decimal() : m_data( 0 )
    {
    }

    explicit Decimal( underlying_type v ) : m_data( v )
    {
    }

    explicit Decimal( double v )
    {
        operator=( v );
    }

    template<class T>
    typename std::enable_if<std::is_floating_point<T>::value, this_type &>::type operator=( T v )
    {
        m_data = static_cast<underlying_type>( std::lround( double( Multiplier ) * v ) );
        return *this;
    }

    template<class T>
    typename std::enable_if<std::is_integral<T>::value, this_type &>::type operator=( T v )
    {
        m_data = static_cast<underlying_type>( Multiplier * v );
        return *this;
    }

    double get_double() const
    {
        return static_cast<double>( m_data ) / Multiplier;
    }

    underlying_type get_integer() const
    {
        return m_data / Multiplier;
    }

    bool is_nan() const
    {
        return HasNAN && NANValue == m_data;
    }

    underlying_type get_raw() const
    {
        return m_data;
    }
    void set_raw( underlying_type v ) const
    {
        m_data = v;
    }

private:
    underlying_type m_data;
};
