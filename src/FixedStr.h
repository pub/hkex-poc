#pragma once
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>
#include <ostream>
#include <sstream>

/// @brief FixedStr<N> Fixed-capacity string.
template<size_t N>
struct FixedStr
{
    static_assert( N > 0, "N > 0" );

    using this_type = FixedStr;

    const static size_t CAPACITY = N;

    FixedStr( const char *pData = nullptr, size_t size = 0 )
    {
        auto actualSize = size < N ? size : ( N - 1 );
        if ( actualSize )
            memcpy( m_data, pData, actualSize );
        m_data[actualSize] = '\0';
    }

    template<size_t M>
    FixedStr( const FixedStr<M> &another ) : FixedStr( another.c_str, another.size() )
    {
    }

    template<size_t M>
    this_type &operator=( const FixedStr<M> &another ) const
    {
        new ( this ) this_type( another );
        return *this;
    }

    template<size_t M>
    this_type &operator=( const char *pData ) const
    {
        size_t n = pData ? strlen( pData ) : 0;
        new ( this ) this_type( pData, n );
        return *this;
    }

    char operator[]( size_t n ) const
    {
        return m_data[n];
    }

    size_t length() const
    {
        return strlen( m_data );
    }

    size_t size() const
    {
        return length();
    }

    size_t capacity() const
    {
        return N;
    }

    const char *c_str() const
    {
        return m_data;
    }

    char *data()
    {
        return m_data;
    }

    bool empty() const
    {
        return m_data[0] == '\0';
    }

    void clear()
    {
        m_data[0] = '\0';
    }

    int printf( const char *format, ... )
    {
        va_list args1;
        va_start( args1, format );
        int nBytes = vsnprintf( m_data, CAPACITY, format, args1 );
        va_end( args1 );
        return nBytes;
    }
    int appendf( const char *format, ... )
    {
        int len = length();
        va_list args1;
        va_start( args1, format );
        int nBytes = vsnprintf( m_data + len, CAPACITY - len, format, args1 ); // vsnprintf doesn't return correct vsnprintf.
        va_end( args1 );
        return nBytes;
    }

    int compare( const char *s, size_t len = ~size_t( 0 ) ) const
    {
        return strncmp( m_data, s, len );
    }

    template<size_t M>
    int compare( const FixedStr<M> &another ) const
    {
        return strcmp( m_data, another.m_data );
    }

    template<size_t M>
    bool operator==( const FixedStr<M> &another ) const
    {
        return compare( another ) == 0;
    }

    template<size_t M>
    bool operator!=( const FixedStr<M> &another ) const
    {
        return !operator==( another );
    }

    template<size_t M>
    bool operator<( const FixedStr<M> &another ) const
    {
        return compare( another ) < 0;
    }

    template<class T>
    this_type &operator<<( const T &v )
    {
        std::stringstream ss;
        ss << v;
        appendf( ss.str().c_str() );
        return *this;
    }

protected:
    char m_data[N] = {'\0'};
} __attribute__( ( packed ) );

template<size_t N>
std::ostream &operator<<( std::ostream &os, const FixedStr<N> &s )
{
    os << s.c_str();
    return os;
}
