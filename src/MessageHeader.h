#pragma once
#include "FixedStr.h"
#include <string>
#include <cassert>
#include "StructCreate.h"

namespace Nxtg
{
namespace HKEX
{

    using ErrMessage = FixedStr<256>;

#define DEINE_MESSAGE_STRUCT( T, MESSAGETYPE )                                                                                                      \
    struct T : public MessageHeader                                                                                                                 \
    {                                                                                                                                               \
        static const auto MESSAGE_TYPE = MessageType::MESSAGETYPE;                                                                                  \
        STRUCT_FIELDS( T )                                                                                                                          \
        Checksum trailer;                                                                                                                           \
    };                                                                                                                                              \
    template<>                                                                                                                                      \
    class Serializer<T> : public MessageSerializer<T>                                                                                               \
    {                                                                                                                                               \
    };


#define DEINE_STRUCT_WITH_BASE( T ) DEINE_MESSAGE_STRUCT( T, T )


#define MessageType_( T, D )                                                                                                                        \
    T( Heartbeat, 0 )                                                                                                                               \
    D T( TestRequest, 1 ) D T( ResendRequest, 2 ) D T( Reject, 3 ) D T( SequenceReset, 4 ) D T( Logon, 5 ) D T( Logout, 6 ) D T( LookupRequest, 7 ) \
            D T( LookupResponse, 8 ) D T( BusinessMessageReject, 9 ) D T( ExecutionReport, 10 ) D T( NewOrder, 11 ) D T( AmendOrder, 12 )           \
                    D T( CancelOrder, 13 ) D T( MassCancel, 14 ) D T( OrderMassCancelReport, 15 ) D T( Quote, 16 ) D T( QuoteCancel, 17 )           \
                            D T( QuoteStatusReport, 18 ) D T( TradeCaptureReport, 21 ) D T( TradeCaptureReportAck, 22 ) D T( OBOCancelRequest, 23 ) \
                                    D T( OBOMassCancelRequest, 24 ) D T( ThrottleEntitlementRequest, 25 ) D T( ThrottleEntitlementResponse, 26 )    \
                                            D T( PartyEntitlementsRequest, 27 ) D T( PartyEntitlementsReport, 28 )

    DEFINE_ENUM_CLASS( MessageType, uint8_t )
    DEFINE_ENUM_STR( MessageType )

    inline std::ostream &operator<<( std::ostream &os, MessageType v )
    {
        return os << to_cstr( v );
    }
    //    enum class MessageType : uint8_t
    //    {
    //        Heartbeat,
    //        TestRequest,
    //        ResendRequest,
    //        Reject,
    //        SequenceReset,
    //        Logon,
    //        Logout,
    //        LookupRequest,
    //        LookupResponse,
    //        BusinessMessageReject,
    //        ExecutionReport,
    //        NewOrder = 11,
    //        AmendOrder,
    //        CancelOrder,
    //        MassCancel,
    //        OrderMassCancelReport,
    //        Quote,
    //        QuoteCancel,
    //        QuoteStatusReport,
    //        NotDefined19,
    //        NotDefined20 = 20,
    //        TradeCaptureReport,
    //        TradeCaptureReportAck,
    //        OBOCancelRequest,
    //        OBOMassCancelRequest,
    //        ThrottleEntitlementRequest,
    //        ThrottleEntitlementResponse,
    //        PartyEntitlementsRequest,
    //        PartyEntitlementsReport,
    //        END_
    //    };

    struct MessageHeader
    {
        enum
        {
            START_OF_MESSAGE = 0x02
        };
        uint8_t startOfMessage = START_OF_MESSAGE; //!< Indicates the starting point of a message. Always set to the ASCII STX character (0x02).
        uint16_t length; //!< length of entire message .
        MessageType messageType;
        uint32_t sequenceNumber;
        uint8_t possDup = 0; //!< Indicates possible retransmission of message with this sequence number.
        uint8_t possResend = 0; //!< Indicates that message may contain information that has been sent under another sequence number:  0 – No
                                //!< (original transmission) ,  1 – Yes (possible resend)
        FixedStr<12> compID;
        BitMap<64> m_fieldPresenceMap;
    } __attribute__( ( packed ) );

    using Checksum = uint32_t; //!< CRC32C based checksum. Polynomial used - 0x1EDC6F41.


    inline MessageHeader *cast_to_message_header( const char *buf, unsigned buflen, ErrMessage *err = nullptr )
    {
        if ( buflen < sizeof( MessageHeader ) )
        {
            if ( err )
            {
                err->appendf( "Buffer too small to read MessageHeader!" );
            }
            return nullptr;
        }
        return reinterpret_cast<MessageHeader *>( const_cast<char *>( buf ) );
    }

} // namespace HKEX
} // namespace Nxtg
