#include "UnitTest.h"
#include "MessageHK.h"
#include "MessageSerializer.h"

using namespace Nxtg;
using namespace Nxtg::HKEX;

template<class T>
struct RemoveRefConst
{
    using type = typename std::remove_reference<typename std::remove_const<T>::type>::type;
};


#define Request_( T, D ) T( id, int ) D T( length, int ) D T( name, std::string )
DEINE_STRUCT( Request )

TEST_FUNC( test_DEINE_STRUCT )
{
    Request req;
    req.set_id( 3 );
    req.set_name( "John" );

    REQUIRE( req.get_name() == "John", << "Wrong name!" );

    std::cout << "\nEnumerate fields:\n";
#if __cplusplus < 201703
#pragma message( "remove this block when c++17 is supported" )
#else
    req.foreach_field( [&]( auto e, auto &v ) {
        if ( req.is_field_set( e ) )
            std::cout << e << ":" << v << ", ";
    } );
#endif
}

#define Color_( T, D ) T( White, 1 ) D T( Red, 2 )
DEFINE_ENUM_CLASS( Color, int )
DEFINE_ENUM_STR( Color )

#define Shape_( T, D ) T( Rectangle ) D T( Triangle )
DEFINE_AUTOENUM( Shape )
DEFINE_ENUM_STR( Shape )

TEST_FUNC( Enum_Tests )
{
    Color c;
    from_cstr( c, "Red" );
    REQUIRE( c == Color::Red );
    REQUIRE( strcmp( to_cstr( c ), "Red" ) == 0 );

    Shape s;
    from_cstr( s, "Triangle" );
    REQUIRE( s == Shape::Triangle );
    REQUIRE( strcmp( to_cstr( s ), "Triangle" ) == 0 );

    {
        REQUIRE( std::string( "typeOfService" ) == LookupRequest::to_cstr( LookupRequest::FieldID::typeOfService ) );

        LookupRequest::FieldID fid;
        REQUIRE( LookupRequest::from_cstr( fid, "typeOfService" ) );
        REQUIRE( LookupRequest::FieldID::typeOfService == fid );
    }
}

TEST_FUNC( test_FixedStr )
{
    enum
    {
        SLEN = 3
    };
    FixedStr<SLEN> s;
    // std::cout << s.length() << ":" << s << std::endl;
    REQUIRE( 1 == s.appendf( "A" ) );
    // std::cout << s.length() << ":" << s << std::endl;
    REQUIRE( 1 == s.appendf( "B" ) );
    REQUIRE( '\0' == s[2] );
    REQUIRE( 2 == s.appendf( "CD" ) ); // @note: should be 0,
    REQUIRE( s.compare( "AB" ) == 0 );
    // std::cout << s.length() << ":" << s << std::endl;
}

TEST_FUNC( Timestamp_Tests )
{
    {
        time_t timet;
        time( &timet );

        auto timeNow = std::chrono::system_clock::now();

        REQUIRE( timet == std::chrono::duration_cast<std::chrono::seconds>( timeNow.time_since_epoch() ).count() );

        // std::cout << "timet: " << timet << " == time_point:" << std::chrono::duration_cast<std::chrono::seconds> (
        // timeNow.time_since_epoch()).count() << std::endl;
    }
    enum
    {
        buflen = 64
    };
    ErrMessage err;
    FixedStr<buflen> buf;
    EpochTime timeNow = std::chrono::system_clock::now(), timeNow1;

    Serializer<EpochTime> serializer;
    serializer.write( timeNow, buf.data(), buflen, err );
    std::cout << "timestamp: " << buf << std::endl;

    REQUIRE( 0 < serializer.read( timeNow1, buf.c_str(), buflen, err ), << err );
    REQUIRE( timeNow.time_since_epoch().count() / 1000000 == timeNow1.time_since_epoch().count() / 1000000 ); // milliseconds
}

TEST_FUNC( LookupRequest_Tests )
{
    enum
    {
        buflen = 2048
    };
    FixedStr<buflen> buf;
    LookupRequest lookupRequest, lookupRequest1;
    ErrMessage err;
    int retcode = 0;

    Serializer<LookupRequest> serializer;
    lookupRequest.messageType = LookupRequest::MESSAGE_TYPE;
    lookupRequest.set_typeOfService( 1 );
    lookupRequest.set_protocolType( 1 );
    REQUIRE( ( retcode = serializer.write( lookupRequest, buf.data(), buflen, err ) ) > 0, << err );
    std::cout << "bytes written:" << retcode << std::endl;

    REQUIRE( cast_to_message_header( buf.c_str(), buflen, &err )->messageType == lookupRequest.messageType, << err );

    REQUIRE( serializer.read( lookupRequest1, buf.data(), buflen, err ) > 0, << err );

#if __cplusplus < 201703
//    REQUIRE( lookupRequest1 == lookupRequest );
#else
    lookupRequest1.foreach_field( [&]( auto fid, const auto &field ) {
        REQUIRE( lookupRequest1.is_field_set( fid ) == lookupRequest.is_field_set( fid ), << "fid existances don't equal " << fid );

        if ( !lookupRequest1.is_field_set( fid ) )
            return;
        //        auto pField = lookupRequest.get_field_by_id<typename RemoveRefConst<decltype( field )>::type>( fid );
        auto pField = reinterpret_cast<typename RemoveRefConst<decltype( field )>::type *>( lookupRequest.get_field_address( fid ) );

        REQUIRE( pField, << "Null Field " << fid );
        REQUIRE( *pField == field );
    } );
#endif
}
